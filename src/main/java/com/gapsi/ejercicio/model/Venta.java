/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gapsi.ejercicio.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jvazquez
 */
@Entity
@Table(name = "Venta")
public class Venta implements Serializable {

	private static final long serialVersionUID = 1530028955840490935L;
	
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@NotNull
	@Column(name = "fecha")
	@Temporal(TemporalType.DATE)
	private Date fecha;
	@NotNull
	@Column(name = "monto")
	private BigDecimal monto;
	@NotNull
	@Column(name = "sucursal")
	private short sucursal;
	@NotNull
	@Size(min = 1, max = 5)
	@Column(name = "cliente")
	private String cliente;
	@NotNull
	@Size(min = 1, max = 3)
	@Column(name = "canal")
	private String canal;
	
	
	public Venta() {
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}
	
	public short getSucursal() {
		return sucursal;
	}

	public void setSucursal(short sucursal) {
		this.sucursal = sucursal;
	}

	
	
}
