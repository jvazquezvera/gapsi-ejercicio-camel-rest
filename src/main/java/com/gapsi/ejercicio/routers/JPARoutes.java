package com.gapsi.ejercicio.routers;

import javax.transaction.Transactional;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Transactional
public class JPARoutes extends RouteBuilder {
	
	static final Logger log = LoggerFactory.getLogger(ApiRouter.class);
	
	@Override
	public void configure() throws Exception {
		  from("direct:insertAll")
		  .log(LoggingLevel.INFO,log,"Preparando los datos para guardarlos en la base de datos")
		  .bean("integrationBean","preparaDatos")
		  .to("jpa://com.gapsi.ejercicio.model.Venta?entityType=java.util.List")
		  .log(LoggingLevel.DEBUG, log,"${body}");
	}

}
