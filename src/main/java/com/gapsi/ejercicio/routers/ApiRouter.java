package com.gapsi.ejercicio.routers;

import javax.ws.rs.core.MediaType;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import com.gapsi.ejercicio.dto.VentaDto;

/**
 * Clase que define el API con Swagger usando Camel Rest DSL.
 */
@Component
public class ApiRouter extends RouteBuilder {
    
	static final Logger log = LoggerFactory.getLogger(ApiRouter.class);
	
    @Autowired
    private Environment env;
    
    @Value("${camel.component.servlet.mapping.context-path}")
    private String contextPath;

    private static final String API_CTX="/api-doc";
    
    @Override
    public void configure() throws Exception {

         restConfiguration()
            .bindingMode(RestBindingMode.json)
            .contextPath(contextPath.substring(0, contextPath.length() - 2))
            .apiContextPath(API_CTX)
            .apiProperty("api.title", "Gapsi Importacion Ventas Legado API")
            .apiProperty("api.version", "1.0.0");

                
        rest("/ventas").description("Servicio Rest para la carga de ventas de sistema legado")
            .consumes(MediaType.APPLICATION_JSON)
            .produces(MediaType.APPLICATION_JSON)
            
              .post("/importacion").type(VentaDto[].class)
              .description("Serivicio que cargal ainformacion necesaria en mongo para el procesmiento de deduplicacion")
              .outType(String.class)
                 .responseMessage().code(200).message("Succes").endResponseMessage()
	             .to("direct:insertAll");
	     
    }

}
