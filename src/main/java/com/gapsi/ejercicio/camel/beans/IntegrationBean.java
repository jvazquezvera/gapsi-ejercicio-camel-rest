package com.gapsi.ejercicio.camel.beans;

import java.util.ArrayList;
import java.util.List;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.gapsi.ejercicio.dto.VentaDto;
import com.gapsi.ejercicio.model.Venta;

@Component("integrationBean")
public class IntegrationBean {

	static final Logger log = LoggerFactory.getLogger(IntegrationBean.class);
	
    
    
	public void preparaDatos(Exchange exchange) {
		@SuppressWarnings("unchecked")
		List<VentaDto> listaEntrada= (List<VentaDto>) exchange.getMessage().getBody();
		log.debug("elementos recibidos..."+listaEntrada);
		List<Venta> listaToInsert= new ArrayList<>();
		
			for(VentaDto ventaDto: listaEntrada) {
				  Venta venta= new Venta();
				  venta.setCanal(ventaDto.getCanal());
				  venta.setCliente(ventaDto.getCliente());
				  venta.setFecha(ventaDto.getFecha());
				  venta.setMonto(ventaDto.getMonto());
				  venta.setSucursal(ventaDto.getSucursal());
				  listaToInsert.add(venta);
			}
	   exchange.getMessage().setBody(listaToInsert);
	}
	
}
