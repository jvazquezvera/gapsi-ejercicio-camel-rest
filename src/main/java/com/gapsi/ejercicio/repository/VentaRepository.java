package com.gapsi.ejercicio.repository;


import org.springframework.data.repository.CrudRepository;

import com.gapsi.ejercicio.model.Venta;



public interface VentaRepository extends CrudRepository<Venta, Integer> {
	  
}
